/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <proc.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <vm.h>
#include <coremap.h>

/*
 * Dumb MIPS-only "VM system" that is intended to only be just barely
 * enough to struggle off the ground.
 */

/* under dumbvm, always have 48k of user stack */
#define DUMBVM_STACKPAGES    12

/*
 * Wrap rma_stealmem in a spinlock.
 */

static struct spinlock stealmem_lock = SPINLOCK_INITIALIZER;
/*
 struct coremap_status{
    paddr_t paddr;
    paddr_t owner;
    volatile bool use;
};

struct coremap{
    struct coremap_status * status;
    
    struct spinlock cl;
    unsigned int num_of_pages;
    
};
*/

/* coremap is definde above */
static struct coremap * cm;
/* set a global varible to record if vm_bootstrap is finished*/
static bool bootstrap = false;

/* During previous bootstrap, the kernel allocates memory by calling getppages,
   which in turn calls ram_stealmem(pages).*/
/* In this vm_bootstrap, call ram_getsize to get the remaining physical memory
   in the system. Do not call ram_stealmem again! */
void vm_bootstrap(void) {
    // kprintf("bootstrap start \n");
    paddr_t stack_bot = 0;
    paddr_t stack_top = 0;
    // kprintf("step1 start \n");
    
    /* Step 1: get the remaining physical memory */
    /* Call ram_getsize to get the remaining physical memory
    in the system. (Do not call ram_stealmem) */
    ram_getsize(&stack_bot,&stack_top);
    
    /* Logically partition the memory into fixed size frames.*/
    /* Each frame is PAGE_SIZE bytes. */
    unsigned long num_of_frame = (stack_top - stack_bot) / PAGE_SIZE;
    
    // kprintf("step2 start \n");
    /* Step 2: core-map data structure */
    /* Keep track of the status of each frame. (core-map data structure)*/
    /* Store it in the start of the memory returned by ram_getsize.
       The frames should start after the core-map data structure.*/
    /* The core-map should track which frames are used and available. It
       should also keep track of contiguous memory allocations*/
    
    /* init core_map */
    cm = kmalloc(sizeof(struct coremap));
    // kprintf("malloc done \n");
    // kprintf("step2 start \n");
    spinlock_init(&cm->cl);
    cm->num_of_pages = num_of_frame ;
    // kprintf("malloc2 start \n");
    cm->status = kmalloc ((sizeof(struct coremap_status)) * num_of_frame);
    // kprintf("malloc2 done \n");
    /* init all frames be owner = 0 and not in used. */
    for(unsigned long i = 0; i < num_of_frame; i ++){
        // kprintf("set owner \n");
        cm->status[i].owner = 0;
        // kprintf("set paddr\n");
        cm->status[i].paddr = stack_bot + (i * PAGE_SIZE);
        // kprintf("set flag \n");
        cm->status[i].use = false;
    }
    // kprintf("init ends \n");
    
    /* Store coremap in the start of the memory returned by ram_getsize. */
    
    /* Update the remain physical memory in the system */
    paddr_t remain_top = 0;
    ram_getsize(&remain_top,&stack_top);
    
    /* Update status */
    for (unsigned long i = 0; cm->status[i].paddr < remain_top; i++){
        cm->status[i].use = true;
    }
    // kprintf("update ends \n");
    bootstrap = true;
    // kprintf("bootstrap ends \n");
}


/* The main idea of getppages is*/
/* before bootstrap, we don't manage memory, we just use ram_stealmem */
/* after boostrap, we need to manage memory ourself, what we do is find n contiguous */
/* pages and once we find n contiguous pages, we set the owner of those mempory be addr */
/* and its in use or not flag be 1 */
static paddr_t getppages(unsigned long npages){
    // kprintf("getppages start \n");
    paddr_t addr = 0;
    // kprintf("acquire lock \n");
    
    /* Before bootstrap */
    if (bootstrap == false){
        spinlock_acquire(&stealmem_lock);
        /* We don't manage memory, we just use ram_stealmem */
        addr = ram_stealmem(npages);
        spinlock_release(&stealmem_lock);
        // kprintf("getppages done 1 \n");
        return addr;
    }
    spinlock_acquire(&cm->cl);
    /* After bootstrap */
    /* We need to manage memory ourself */
    /* Iterates through the core_map's status to find n contiguous pages */
    for(unsigned long i = 0; i < cm->num_of_pages; ++i){
        unsigned long free_block = 0;
        if(!cm->status[i].use){
            for (unsigned long j = i; j < cm->num_of_pages; ++j){
                if(!cm->status[j].use) {
                    ++free_block;
                }
                // kprintf("n contiguous pages  found\n");
                /* This means from page i to page j, we have n contiguous
                   pages are not in used */
                if(free_block == npages){
                    addr = cm->status[i].paddr;
                    /* Update status */
                    for (unsigned long k = i; k <= j; ++k){
                        /* A page's owner is the first block's physical address */
                        cm->status[k].owner = cm->status[i].paddr;
                        cm->status[k].use = 1;
                    }
                    // kprintf("Update done\n");
                    spinlock_release(&cm->cl);
                    return addr;
                }
                /* No n contiguous vaild pages from i to j, break */
                // kprintf("does not exist n contiguous pages\n");
                if(cm->status[j].use){
                    break;
                }
            }
        }
    }
    /* We didn't has n contiguous vaild page in system, just return 0 */
    spinlock_release(&cm->cl);
    // kprintf("getppages done 2 \n");
    return 0;
}


/* Allocate/free some kernel-space virtual pages */
vaddr_t 
alloc_kpages(int npages)
{
	paddr_t pa;
	pa = getppages(npages);
	if (pa==0) {
		return 0;
	}
	return PADDR_TO_KVADDR(pa);
}

/* The main idea of page release is find the corresponding
   item in  coremap and set the owner be zero and set the in
   use flag be zero*/
static void Page_Release(paddr_t paddr){
    spinlock_acquire(&cm->cl);
    /* Iterates through the core_map's status to find the
       corresponding item */
    for (unsigned long i = 0; i < cm->num_of_pages; i ++){
        if(cm->status[i].paddr == paddr && cm->status[i].owner == paddr){
            // kprintf("The corresponding item found\n");
            unsigned long j = i;
            /* keep update until we does not own next page*/
            while (cm->status[j].owner == paddr){
                cm->status[j].owner = 0;
                cm->status[j].use = 0;
                ++j;
            }
            // kprintf("Update done\n");
            spinlock_release(&cm->cl);
            return;
        }
    }
    spinlock_release(&cm->cl);
}

void free_kpages(vaddr_t addr){
    // kprintf("free start\n");
    Page_Release(addr);
    // kprintf("free ends\n");
}


void
vm_tlbshootdown_all(void)
{
	panic("dumbvm tried to do tlb shootdown?!\n");
}

void
vm_tlbshootdown(const struct tlbshootdown *ts)
{
	(void)ts;
	panic("dumbvm tried to do tlb shootdown?!\n");
}

/* VM related exceptions are handled by vm_fault */
/* vm_fault performs address translation and loads the virtual
   address to physical address mapping into the TLB.*/
int
vm_fault(int faulttype, vaddr_t faultaddress)
{
	vaddr_t vbase1, vtop1, vbase2, vtop2, stackbase, stacktop;
	paddr_t paddr;
	int i;
	uint32_t ehi, elo;
	struct addrspace *as;
	int spl;
    /* READONLY_SEG means is this a Read-Only segment */
    bool READONLY_SEG = false;
    
    
	faultaddress &= PAGE_FRAME;

	DEBUG(DB_VM, "dumbvm: fault: 0x%x\n", faultaddress);

	switch (faulttype) {
	    case VM_FAULT_READONLY:
            /* Now we can get this*/
             return EFAULT;
	    case VM_FAULT_READ:
	    case VM_FAULT_WRITE:
		break;
	    default:
		return EINVAL;
	}

	if (curproc == NULL) {
		/*
		 * No process. This is probably a kernel fault early
		 * in boot. Return EFAULT so as to panic instead of
		 * getting into an infinite faulting loop.
		 */
		return EFAULT;
	}

	as = curproc_getas();
	if (as == NULL) {
		/*
		 * No address space set up. This is probably also a
		 * kernel fault early in boot.
		 */
		return EFAULT;
	}

	/* Assert that the address space has been set up properly. */
	KASSERT(as->as_vbase1 != 0);
	KASSERT(as->as_pbase1 != 0);
	KASSERT(as->as_npages1 != 0);
	KASSERT(as->as_vbase2 != 0);
	KASSERT(as->as_pbase2 != 0);
	KASSERT(as->as_npages2 != 0);
	KASSERT(as->as_stackpbase != 0);
	KASSERT((as->as_vbase1 & PAGE_FRAME) == as->as_vbase1);
	KASSERT((as->as_pbase1 & PAGE_FRAME) == as->as_pbase1);
	KASSERT((as->as_vbase2 & PAGE_FRAME) == as->as_vbase2);
	KASSERT((as->as_pbase2 & PAGE_FRAME) == as->as_pbase2);
	KASSERT((as->as_stackpbase & PAGE_FRAME) == as->as_stackpbase);

	vbase1 = as->as_vbase1;
	vtop1 = vbase1 + as->as_npages1 * PAGE_SIZE;
	vbase2 = as->as_vbase2;
	vtop2 = vbase2 + as->as_npages2 * PAGE_SIZE;
	stackbase = USERSTACK - DUMBVM_STACKPAGES * PAGE_SIZE;
	stacktop = USERSTACK;
    
    /* paddr convesion start here */
    /* Determine the segment of the fault address by looking at the vbase and vtop addresses */
	if (faultaddress >= vbase1 && faultaddress < vtop1) {
        /* vbase1 is the base virtual address of code segment, this indicate READONLY_SEG = true*/
        READONLY_SEG = true;
		paddr = (faultaddress - vbase1) + as->as_pbase1;
	}
	else if (faultaddress >= vbase2 && faultaddress < vtop2) {
		paddr = (faultaddress - vbase2) + as->as_pbase2;
	}
	else if (faultaddress >= stackbase && faultaddress < stacktop) {
		paddr = (faultaddress - stackbase) + as->as_stackpbase;
	}
	else {
		return EFAULT;
	}
    /* paddr convesion ends here */
    
    
	/* Make sure it's page-aligned */
	KASSERT((paddr & PAGE_FRAME) == paddr);

	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();
    
    // kprintf("find unsused entry\n");
    /* Iterates through the TLB to find an unused/invalid entry */
    /* Currently, TLB entries are loaded with TLBLO_DIRTY on */
    /* Pages are therefore read and writeable */
	for (i=0; i < NUM_TLB; i++) {
		tlb_read(&ehi, &elo, i);
		if (elo & TLBLO_VALID) {
			continue;
		}
        /* If we reach here, means we already find an unused/invalid entry */
        // kprintf("has unsused entry\n");
		ehi = faultaddress;
		elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
        
        /* Text segment should be read-only */
        /* We must instead load TLB entries with TLBLO_DIRTY on until load_elf has completed.*/
        /* Adding a flag to struct addrspace to indicate whether or not load_elf has completed*/
        if(READONLY_SEG && as->complete){
            elo &= ~TLBLO_DIRTY;
        }

		DEBUG(DB_VM, "dumbvm: 0x%x -> 0x%x\n", faultaddress, paddr);
        /* Overwrites the unused entry with the virtual to physical address
           mapping required by the instruction that generated the TLB exception */
		tlb_write(ehi, elo, i);
		splx(spl);
        // kprintf("tlb write\n");
		return 0;
	}
    
    /* If the TLB is full, call tlb_random to write the entry into a random TLB slot */
    /* This is for TLB replacement */
    ehi = faultaddress;
    elo = paddr | TLBLO_DIRTY | TLBLO_VALID;
    
    /* We must instead load TLB entries with TLBLO_DIRTY on until load_elf has completed.*/
    if(READONLY_SEG && as->complete){
        elo &= ~TLBLO_DIRTY;
    }
    
    tlb_random(ehi,elo);
    splx(spl);
    return 0;
}

struct addrspace *
as_create(void)
{
	struct addrspace *as = kmalloc(sizeof(struct addrspace));
	if (as==NULL) {
		return NULL;
	}

	as->as_vbase1 = 0;
	as->as_pbase1 = 0;
	as->as_npages1 = 0;
	as->as_vbase2 = 0;
	as->as_pbase2 = 0;
	as->as_npages2 = 0;
	as->as_stackpbase = 0;
    
    /* set the field be not complete*/
    as->complete = false;
    
	return as;
}

void
as_destroy(struct addrspace *as)
{
    /* Call free_kpages on the frames for each segment */
    Page_Release(as->as_stackpbase);
    Page_Release(as->as_pbase2);
    Page_Release(as->as_pbase1);
/*
	kfree(as);
 */
}

void
as_activate(void)
{
	int i, spl;
	struct addrspace *as;

	as = curproc_getas();
#ifdef UW
        /* Kernel threads don't have an address spaces to activate */
#endif
	if (as == NULL) {
		return;
	}

	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();

	for (i=0; i<NUM_TLB; i++) {
		tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
	}

	splx(spl);
}

void
as_deactivate(void)
{
	/* nothing */
}

int
as_define_region(struct addrspace *as, vaddr_t vaddr, size_t sz,
		 int readable, int writeable, int executable)
{
	size_t npages; 

	/* Align the region. First, the base... */
	sz += vaddr & ~(vaddr_t)PAGE_FRAME;
	vaddr &= PAGE_FRAME;

	/* ...and now the length. */
	sz = (sz + PAGE_SIZE - 1) & PAGE_FRAME;

	npages = sz / PAGE_SIZE;

	/* We don't use these - all pages are read-write */
	(void)readable;
	(void)writeable;
	(void)executable;

	if (as->as_vbase1 == 0) {
		as->as_vbase1 = vaddr;
		as->as_npages1 = npages;
		return 0;
	}

	if (as->as_vbase2 == 0) {
		as->as_vbase2 = vaddr;
		as->as_npages2 = npages;
		return 0;
	}

	/*
	 * Support for more than two regions is not available.
	 */
	kprintf("dumbvm: Warning: too many regions\n");
	return EUNIMP;
}

static
void
as_zero_region(paddr_t paddr, unsigned npages)
{
	bzero((void *)PADDR_TO_KVADDR(paddr), npages * PAGE_SIZE);
}

int
as_prepare_load(struct addrspace *as)
{
	KASSERT(as->as_pbase1 == 0);
	KASSERT(as->as_pbase2 == 0);
	KASSERT(as->as_stackpbase == 0);

	as->as_pbase1 = getppages(as->as_npages1);
	if (as->as_pbase1 == 0) {
		return ENOMEM;
	}

	as->as_pbase2 = getppages(as->as_npages2);
	if (as->as_pbase2 == 0) {
		return ENOMEM;
	}

	as->as_stackpbase = getppages(DUMBVM_STACKPAGES);
	if (as->as_stackpbase == 0) {
		return ENOMEM;
	}
	
	as_zero_region(as->as_pbase1, as->as_npages1);
	as_zero_region(as->as_pbase2, as->as_npages2);
	as_zero_region(as->as_stackpbase, DUMBVM_STACKPAGES);

	return 0;
}

int
as_complete_load(struct addrspace *as)
{
    /* set the flag be completed */
    as->complete = true;
    
    for (int i=0; i<NUM_TLB; i++)
    {
        tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
    }

    return 0;
    /*
	(void)as;
	return 0;
     */
}

int
as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{
	KASSERT(as->as_stackpbase != 0);

	*stackptr = USERSTACK;
	return 0;
}

int
as_copy(struct addrspace *old, struct addrspace **ret)
{
	struct addrspace *new;

	new = as_create();
	if (new==NULL) {
		return ENOMEM;
	}

	new->as_vbase1 = old->as_vbase1;
	new->as_npages1 = old->as_npages1;
	new->as_vbase2 = old->as_vbase2;
	new->as_npages2 = old->as_npages2;

	/* (Mis)use as_prepare_load to allocate some physical memory. */
	if (as_prepare_load(new)) {
		as_destroy(new);
		return ENOMEM;
	}

	KASSERT(new->as_pbase1 != 0);
	KASSERT(new->as_pbase2 != 0);
	KASSERT(new->as_stackpbase != 0);

	memmove((void *)PADDR_TO_KVADDR(new->as_pbase1),
		(const void *)PADDR_TO_KVADDR(old->as_pbase1),
		old->as_npages1*PAGE_SIZE);

	memmove((void *)PADDR_TO_KVADDR(new->as_pbase2),
		(const void *)PADDR_TO_KVADDR(old->as_pbase2),
		old->as_npages2*PAGE_SIZE);

	memmove((void *)PADDR_TO_KVADDR(new->as_stackpbase),
		(const void *)PADDR_TO_KVADDR(old->as_stackpbase),
		DUMBVM_STACKPAGES*PAGE_SIZE);
	
	*ret = new;
	return 0;
}
