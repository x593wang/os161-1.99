#ifndef _COREMAP_H_
#define _COREMAP_H_

#include <spinlock.h>
struct coremap_status{
    paddr_t paddr;
    paddr_t owner;
    volatile bool use;
};

struct coremap{
    struct coremap_status * status;
    
    struct spinlock cl;
    unsigned int num_of_pages;
    
};

static void Page_Release(paddr_t paddr);

#endif
