#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <kern/syscall.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include <vfs.h>
#include "opt-A2.h"

#if OPT_A2
void fork_entry_function(void *data1, unsigned long data2){
    (void) data2;
    
    struct trapframe * child_trapframe = (struct trapframe*) data1;
    enter_forked_process(child_trapframe);
}

int sys_fork(struct trapframe *tf,pid_t *retval){
/* Create process structure for child process */
    /* Use proc_create_runprogram(…) to create the process structure this
       will init process's field and alloc memory for child process */
    struct proc *child = proc_create_runprogram(curproc->p_name);
    
    if(child == NULL) return ENOMEM;
    
    int err = 0;
    /* We can get child's pid after initization */
    int cpid = child->pid;
    
    if (cpid <= 0){
        proc_destroy(child);
        return err;
    }
/* Create and copy address space */
    /* as_copy() creates a new address spaces, and copies the
    pages from the old address space to the new one */
    err = as_copy(curproc->p_addrspace,&(child->p_addrspace));
    /* Handle any error conditions */
    if(err){
        proc_destroy(child);
        return err;
    }
                                                                                    
/* Create the parent/child relationship*/
    
    curproc->childPids[curproc->child_counter] = cpid;
    curproc->child_counter += 1;

/* Create thread for child process */
    /* Need to pass trapframe to the child thread (cannot just pass pointer,
     we use memcpy here) */
    struct trapframe * ctrapframe = NULL;
    ctrapframe = kmalloc(sizeof(struct trapframe));
    if(ctrapframe == NULL){
        err = ENOMEM;
        return err;
    }
    memcpy(ctrapframe, tf, sizeof(struct trapframe));
    /* Use thread_fork() to create a new thread */
    err = thread_fork("process", child, fork_entry_function, ctrapframe, 0);
    
    if(err){
        proc_destroy(child);
        return err;
    }
    *retval = cpid;
    
    return 0;
}
#endif

/* stub handler for getpid() system call                */
int sys_getpid(pid_t *retval) {
#if OPT_A2
    *retval = curproc->pid;
    return(0);
#else
    *retval = 1;
    return(0);
#endif
}


  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
  (void)exitcode;

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
#if OPT_A2
    /* Write current process's exit info and wake porcess wait for this process*/
    lock_acquire(proc_lk);
    
    /* In this assignment, exit status should always be
    __WEXITED. */
    curproc->Status = __WEXITED;
    curproc->State = 1;
    
    Exit_Info[curproc->pid - __MIN_PID] = exitcode;
    cv_broadcast(curproc->pcv, proc_lk);
        
    lock_release(proc_lk);
#endif
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
  proc_destroy(p);
  
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


#if OPT_A2
int Exit_Status(pid_t pid, int options){
    KASSERT(Procs[pid - __MIN_PID]->pcv != NULL);
    KASSERT(Procs[pid - __MIN_PID] != NULL);
    
    int status = 0;
    int exitcode = 0;
    
    lock_acquire(proc_lk);
    
    while (Procs[pid - __MIN_PID]->State == 2) {
        if (options == WNOHANG) {       //  WNOHANG option
            lock_release(proc_lk);
            return ECHILD;
        } else {                        // If still ailve, wait
            cv_wait(Procs[pid - __MIN_PID]->pcv, proc_lk);
        }
    }

    exitcode = Exit_Info[pid - __MIN_PID];
    status = _MKWAIT_EXIT(exitcode);
    
    lock_release(proc_lk);
    
    return status;
}
#endif

int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
#if OPT_A2
    int exitstatus;
    int result;
    
    if (options != 0) {
          return(EINVAL);
    }
    /* handle the case that pid <= 0*/
    if (pid <= 0) {
        * retval = ESRCH;
        return -1;
    }
    /* handle parent did not call waitpid on its children*/
    bool My_child = false;
    for (int i = 0; i < curproc->child_counter; i ++){
        if (pid == curproc->childPids[i]) {
            My_child = true;
            break;
        }
    }
    if(My_child == false){
        * retval = ECHILD;
        return -1;
    }
 
    exitstatus = Exit_Status(pid, options);
    result = copyout((void *)&exitstatus,status,sizeof(int));
    if (result) {
        return(result);
    }
    *retval = pid;
    return(0);
    
#else
    int exitstatus;
    int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

    if (options != 0) {
        return(EINVAL);
    }
    /* for now, just pretend the exitstatus is 0 */
    exitstatus = 0;
    result = copyout((void *)&exitstatus,status,sizeof(int));
    if (result) {
        return(result);
    }
    *retval = pid;
    return(0);
#endif
}



/* To finish execv, we need to finish follow 8 steps (from assignment 2b review) */
/* * Count the number of arguments and copy them into the kernel */
/* * Copy the program path into the kernel */
/* * Open the program file using vfs_open(prog_name, ...) */
/* * Create new address space, set process to the new address space, and activate it */
/* * Using the opened program file, load the program image using load_elf */
/* * Need to copy the arguments into the new address space.(both the array and the strings)
      Consider copying the arguments (both the array and the strings) onto the user stack as part of as_define_stack.*/
/* * Delete old address space. */
/* * Call enter_new_process with address to the arguments on the stack, the stack pointer (from as_define_stack), and the program entry point (from vfs_open) */

/* Note: the code surround by - is just the copy of runprogram.c */


int sys_execv(char *progname, char **args) {
    
    
    /*   /---------------Copy of runprogram.c-----------------\   */
    /* Copy of the line 57 - 60 in runprogram.c */
    struct addrspace *as;
    struct vnode *v;
    vaddr_t entrypoint, stackptr;
    int result;
    /*   \---------------Copy of runprogram.c-----------------/   */
    
    
    
    /* * Count the number of arguments and copy them into the kernel */
    /* num_arg = the # of arguments */
    int num_arg = 0;
    while(args[num_arg] != NULL) {
        num_arg += 1;
    }
    /* argumnets = the array of atguments need to be copied into kernel */
    char ** argumnets = (char **) kmalloc((num_arg + 1) * sizeof(char *));
    if(argumnets == NULL) {
        return ENOMEM;
    }
    
    for(int i = 0; i < num_arg; i ++){
        /* malloc the space for each arg in arguments */
        size_t len = strlen(args[i]) + 1;
        argumnets[i] = (char *) kmalloc(len * sizeof(char));
        
        /* handle the case that argumnets[i] = NULL*/
        if(argumnets[i] == NULL){
            for(int j = 0; j < i; j ++ ) {
                kfree(argumnets[j]);
            }
            kfree(argumnets);
            return ENOMEM;  // this cause a no memory error
        }
        
        /* Copy args into kernel */
        result = copyin((const_userptr_t)args[i], argumnets[i], len * sizeof(char)); // Copy args into kernel
        /* handle the case copyin returns error */
        if(result){
            /* only free the memory before i */
            for(int j = 0; j <= i; ++j) {
                kfree(argumnets[j]);
            }
            kfree(argumnets);
            return result;
        }
    }
    /* let the last index = NULL */
    argumnets[num_arg] = NULL;
    
    
    
    
    /* * Copy the program path into the kernel */
    /* pname_len = the length of progname */
    size_t pname_len = strlen(progname) + 1;
    /* pname_to_kernel = the progname need to be copied into kernel */
    char * pname_to_kernel =  (char *) kmalloc(pname_len * sizeof(char));
    /* handle the case that pname_to_kernel = NULL */
    if(pname_to_kernel == NULL) {
        for(int i = 0; i < num_arg; i ++ ) {
            kfree(argumnets[i]);
        }
        kfree(argumnets);
        return ENOMEM;
    }
    /* Copy the program path into the kernel */
    result = copyin((const_userptr_t)progname, (void *) pname_to_kernel, pname_len * sizeof(char));
    /* handle the case copyin returns error */
    if(result) {
        for(int i = 0; i < num_arg; i ++ ) {
            kfree(argumnets[i]);
        }
        kfree(argumnets);
        kfree(pname_to_kernel);
        return result;
    }
    
    
    
    /*   /---------------Copy of runprogram.c-----------------\   */
    /* Copy of the line 52 - 66 in runprogram.c */
    /* * Opens the program file using vfs_open(progname, ...) */
    /* Open the file. */
    result = vfs_open(pname_to_kernel, O_RDONLY, 0, &v);
    if (result) {
        for(int i = 0; i < num_arg; i ++ ) {
            kfree(argumnets[i]);
        }
        kfree(argumnets);
        kfree(pname_to_kernel);
        return result;
    }

    
    
    
    
    /* Copy of the line 71 - 80 in runprogram.c */
    /* * Creates a new address space (as_create), switches the
       process to that address space (curproc_setas) and then
       activates it (as_activate). */
    
    /* Create a new address space. */
    as = as_create();
    if (as == NULL) {
        for(int i = 0; i < num_arg; i ++ ) {
            kfree(argumnets[i]);
        }
        kfree(argumnets);
        kfree(pname_to_kernel);
        vfs_close(v);
        return ENOMEM;
    }
    /* Switch to it and activate it. */
    struct addrspace * old_address_space = curproc_setas(as);
    as_activate();

    
    
    
    
    /* Copy of the line 82 - 91 in runprogram.c */
    /* * Using the opened program file, load the program image
    using load_elf */
    
    /* Load the executable. */
    result = load_elf(v, &entrypoint);
    if (result) {
        for(int i = 0; i < num_arg; i ++ ) {
            kfree(argumnets[i]);
        }
        kfree(argumnets);
        kfree(pname_to_kernel);
        /* p_addrspace will go away when curproc is destroyed */
        vfs_close(v);
        return result;
    }
    /* Done with the file now. */
    vfs_close(v);
    /*   \---------------Copy of runprogram.c-----------------/   */
    
    kfree(pname_to_kernel);
    
    
    
    
    
    /* * Need to copy the arguments into the new address space.(both the array and the strings)
         Consider copying the arguments (both the array and the strings) onto the user stack as
         part of as_define_stack.*/
    /* Define the user stack in the address space */
    result = as_define_stack(as, &stackptr);
    if (result) {
        for(int i = 0; i < num_arg; i ++ ) {
            kfree(argumnets[i]);
        }
        kfree(argumnets);
        return result;
    }
    /* When storing items on the stack, pad each item */
    int padded_size = 0;
    
    /* Calculate total length accounting for padding */
    for (int i = 0 ;i < num_arg; i ++) {
        int arg_len = strlen(argumnets[i]) + 1;
        /* pointers to strings need to be 4-byte aligned. */
        int padded_item_size = ROUNDUP(arg_len, 4);
        padded_size += (padded_item_size * sizeof(char));
    }
    /* Now we reach the top of the Argument strings */
    stackptr -= padded_size;
    
    /* Copy Arguments string into user stack */
    vaddr_t * argsptr = (vaddr_t *) kmalloc(sizeof(vaddr_t) * (num_arg + 1));
     for (int i = 0 ;i < num_arg; i ++) {
         int arg_len = strlen(argumnets[i]) + 1;
         int padded_item_size = ROUNDUP(arg_len, 4);
         
         argsptr[i] = stackptr;
         copyout((void *) argumnets[i], (userptr_t)stackptr, arg_len);
         stackptr += (padded_item_size * sizeof(char));
     }
    argsptr[num_arg] = (vaddr_t)NULL;
    
    /* Go to the top of the user stack */
    stackptr -= padded_size;
    stackptr -= (num_arg + 1) * sizeof(vaddr_t);
    
    /* Copy Argument array into user stack */
    for (int i = 0; i <= num_arg; i++) {
        copyout((void *) (argsptr + i), (userptr_t)stackptr, sizeof(vaddr_t));
        stackptr += sizeof(vaddr_t);
    }
    
    stackptr -= ((num_arg + 1) * sizeof(vaddr_t));
    kfree(argsptr);
    
    
    
    
    /* * Delete old address space. */
    for(int i = 0; i < num_arg; i ++ ) {
        kfree(argumnets[i]);
    }
    kfree(argumnets);
    
    as_destroy(old_address_space);
    
    
    
    
    /* * Call enter_new_process with address to the arguments on the stack,
         the stack pointer (from as_define_stack), and the program entry
         point (from vfs_open) */
    enter_new_process(num_arg, (userptr_t) stackptr, stackptr, entrypoint); // Warp to user mode.
    
    /* Enter_new_process does not return. */
    panic("enter_new_process returned\n");
    return EINVAL;
}
