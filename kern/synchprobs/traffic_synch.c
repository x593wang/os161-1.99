#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <opt-A1.h>

/* 
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is used as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */

struct lock * mutex_lock;
static struct cv * destination_cv[4];
static int direction[4][4];
typedef struct Vehicles
{
  Direction origin;
  Direction destination;
} Vehicle;
static bool right_turn(Vehicle *v);
/* 
 * The simulation driver will call this function once before starting
 * the simulation
 *
 * You can use it to initialize synchronization and other variables.
 * 
 */

void
intersection_sync_init (void) {
    mutex_lock = lock_create("mutex_lock");
    
    if (mutex_lock == NULL) {
        panic("catmouse_sync_init: mutex_lock creation failed\n");
    }
    
    for (unsigned int i=0; i < 4; i++) {
        destination_cv[i] = cv_create("destination_cv");
        if (destination_cv[i] == NULL) {
            panic("destination_cv creation failed\n");
        }
    }
    
    for (unsigned int i=0; i < 4; i++) {
        for (unsigned int j=0; j < 4; j++) {
            direction[i][j] = 0;
        }
    }
    return;
}

/* 
 * The simulation driver will call this function once after
 * the simulation has finished
 *
 * You can use it to clean up any synchronization and other variables.
 *
 */

void
intersection_sync_cleanup (void) {
    if (mutex_lock != NULL) {
        lock_destroy(mutex_lock);
        mutex_lock = NULL;
    }
    for (unsigned int i=0; i < 4; i++) {
        if (destination_cv[i] != NULL) {
            cv_destroy(destination_cv[i]);
        }
    }
    for (unsigned int i=0; i < 4; i++) {
        for (unsigned int j=0; j < 4; j++) {
                direction[i][j] = 0;
        }
    }
}

/*
 typedef struct Vehicles
 {
   Direction origin;
   Direction destination;
 } Vehicle;

 */

bool
right_turn(Vehicle *v) {
  KASSERT(v != NULL);
  if (((v->origin == west) && (v->destination == south)) ||
      ((v->origin == south) && (v->destination == east)) ||
      ((v->origin == east) && (v->destination == north)) ||
      ((v->origin == north) && (v->destination == west))) {
    return true;
  } else {
    return false;
  }
}

bool block_entrance(Direction origin, Direction destination){
    Vehicle v = {origin, destination};
    
    for (unsigned int i=0; i<4; ++i) {
        for (unsigned int j=0; j<4; ++j) {
            if (direction[i][j] > 0) {
                if (direction[i][j] > 5) return false;
                if (origin == i && destination == j) {
                    continue;
                } else if (origin == j && destination == i) {
                    continue;
                } else if ((destination != j) && right_turn(&v)) {
                    continue;
                }
                else {
                    return true;
                }
            }
        }
    }
    return false;
}

/*
 * The simulation driver will call this function each time a vehicle
 * tries to enter the intersection, before it enters.
 * This function should cause the calling simulation thread 
 * to block until it is OK for the vehicle to enter the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle is arriving
 *    * destination: the Direction in which the vehicle is trying to go
 *
 * return value: none
 */

void
intersection_before_entry (Direction origin, Direction destination)
{
    KASSERT(origin != destination);
    
    lock_acquire(mutex_lock);
    
    while (block_entrance(origin,destination)) {
        cv_wait(destination_cv[destination], mutex_lock);
    }
    direction[origin][destination]++;
    
    lock_release(mutex_lock);
}

/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle arrived
 *    * destination: the Direction in which the vehicle is going
 *
 * return value: none
 */

void
intersection_after_exit(Direction origin, Direction destination)
{
    KASSERT(origin != destination);
    
    lock_acquire(mutex_lock);
    
    direction[origin][destination]--;
    
    if (direction[origin][destination] == 0) {
        cv_broadcast(destination_cv[destination], mutex_lock);
    }
    lock_release(mutex_lock);
}
